<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/Hello-laravel', function(){
//      echo "Ini adalah halaman baru<br>";
//      return "Hello Laravel";
//  });

// // Route::get('/index', function(){
// //     return view('index');
// // });

// // Route::get('/form', function(){
// //     return view('form');
// // });
// // Route::get('/welcome', function(){
// //     return view('welcome');
// // });

// // Route::get('/post', 'PostController@home');

// Route::get('/Register', 'PostController@form');
// Route::post('/welcome', 'PostController@welcome');
Route::get('/', function () {
	return view('items.index');
});

Route::get('/data-table', function () {
	return view('items.data-table');
});