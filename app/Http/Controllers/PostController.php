<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostController extends Controller
{
    //
    public function home(){
        return view('home');
    }

    public function form(){
        return view('form');
    }

    public function welcome(Request $request){
        dd($request ->all());
        return "view('welcome')";
    }
}
